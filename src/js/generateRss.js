module.exports = function (node) {
  let firstCommit = node.gitData[node.gitData.length - 1];
  return {
    id: node.codename,
    title: node.name,
    date: firstCommit.authorDate,
    description: "New device added to the devices website, check it out.",
    content:
      "<p>" +
      node.name +
      " was just added to the devices page.</p>" +
      (node.description ? "<p>" + node.description + "</p>" : "") +
      "<p>" +
      node.progressStage.description +
      "</p>" +
      "<p>Added by " +
      firstCommit.authorName +
      "</p>",
    image: "https://devices.ubuntu-touch.io/social-preview.jpg",
    link: "https://devices.ubuntu-touch.io" + node.path,
    author: {
      name: "UBports contributors",
      link: "https://ubports.com/"
    }
  };
};
